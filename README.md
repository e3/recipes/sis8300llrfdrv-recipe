# sis8300llrfdrv conda recipe

Home: https://gitlab.esss.lu.se/epics-modules/sis8300llrfdrv

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS sis8300llrfdrv module
